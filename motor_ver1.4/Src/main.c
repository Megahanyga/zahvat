/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2020 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "main.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
#define cs_set_1() HAL_GPIO_WritePin(CS1_GPIO_Port, CS1_Pin, GPIO_PIN_RESET)
#define cs_reset_1() HAL_GPIO_WritePin(CS1_GPIO_Port, CS1_Pin, GPIO_PIN_SET)


#define cs_set_2() HAL_GPIO_WritePin(CS2_GPIO_Port, CS2_Pin, GPIO_PIN_RESET)
#define cs_reset_2() HAL_GPIO_WritePin(CS2_GPIO_Port, CS2_Pin, GPIO_PIN_SET)


#define cs_set_3() HAL_GPIO_WritePin(CS3_GPIO_Port, CS3_Pin, GPIO_PIN_RESET)
#define cs_reset_3() HAL_GPIO_WritePin(CS3_GPIO_Port, CS3_Pin, GPIO_PIN_SET)


#define cs_set_4() HAL_GPIO_WritePin(CS4_GPIO_Port, CS4_Pin, GPIO_PIN_RESET)
#define cs_reset_4() HAL_GPIO_WritePin(CS4_GPIO_Port, CS4_Pin, GPIO_PIN_SET)

#define HI 1 
#define LOW 0 

#define RASSTOIANIE_MEGHDY_LUCHAMI 100
#define MIN_UGL_STEPPER 1.8  
#define KOL_MICROSTEP 32  
#define KOL_ZUBOV_SHESTERNI_OX 20
#define KOL_ZUBOV_SHESTERNI_OY 16
#define SHAG_REMNIA 20 
#define RASTOIANIE_OBOROTA_OY (KOL_ZUBOV_SHESTERNI_OY*SHAG_REMNIA) 
#define RASTOIANIE_OBOROTA_OX (KOL_ZUBOV_SHESTERNI_OX*SHAG_REMNIA)  
#define STEP_N_OBOROT (360/(MIN_UGL_STEPPER/KOL_MICROSTEP))*2
#define N_STEP_MM_OX (STEP_N_OBOROT/RASTOIANIE_OBOROTA_OX) 
#define N_STEP_MM_OY (STEP_N_OBOROT/RASTOIANIE_OBOROTA_OY)

#define ID_MOTOR 0xA0
#define ID_DISCR 0x30
#define ID_MOTOR_ANSVER 0x4A0

/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
CAN_HandleTypeDef hcan;

SPI_HandleTypeDef hspi2;

TIM_HandleTypeDef htim1;
TIM_HandleTypeDef htim2;
TIM_HandleTypeDef htim3;
TIM_HandleTypeDef htim4;

UART_HandleTypeDef huart1;

/* USER CODE BEGIN PV */
uint8_t spiTXbuf[5]={0}; 
uint8_t spiRXbuf[5]={0};  

uint64_t step_1_count=0; 
uint64_t step_1_n_end=0; 
uint8_t step_1_flag=LOW; 

uint64_t step_2_count=0; 
uint64_t step_2_n_end=0; 
uint8_t step_2_flag=LOW; 

uint64_t step_3_count=0; 
uint64_t step_3_n_end=0; 
uint8_t step_3_flag=LOW; 

uint64_t step_4_count=0; 
uint64_t step_4_n_end=0; 
uint8_t step_4_flag=LOW; 

int16_t x_1_max=3200; 
int16_t x_1_min=-3200; 
int16_t x_2_max=-3200; 
int16_t x_2_min=3200; 

uint16_t y_1_max=2500; 
uint16_t y_1_min=100; 
uint16_t y_2_max=2500; 
uint16_t y_2_min=100; 

int16_t data_x1; 
int16_t data_x2; 
uint16_t data_y1; 
uint16_t data_y2; 

int16_t x_1=0; 
int16_t x_2=0; 
uint16_t y_1=0; 
uint16_t y_2=0; 

int16_t offset_x1=0; 
int16_t offset_x2=0; 
int16_t offset_y1=0; 
int16_t offset_y2=0;

int8_t flag_Din1=0;
int8_t flag_Din2=0;
int8_t flag_Din3=0;
int8_t flag_Din4=0;

CAN_FilterTypeDef sFilterConfig; 
CAN_TxHeaderTypeDef pHeader; 
CAN_RxHeaderTypeDef RxHeader;
uint32_t TxMailbox;

uint8_t CAN_Txdata[8]; 
uint8_t CAN_Rxdata_save_kord[8]; 
uint8_t CAN_Rxdata[8];
uint8_t CAN_Rxdata_save_Din[8];

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_CAN_Init(void);
static void MX_SPI2_Init(void);
static void MX_TIM4_Init(void);
static void MX_USART1_UART_Init(void);
static void MX_TIM1_Init(void);
static void MX_TIM2_Init(void);
static void MX_TIM3_Init(void);
/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */
void spi_tr_m1 (void)
{
    cs_set_1();//low
    HAL_Delay(10);
    HAL_SPI_Transmit(&hspi2,spiTXbuf,5,10);
    HAL_Delay(10);
    cs_reset_1();// hight
}

void spi_tr_m2 (void)
{
    cs_set_2();//low
    HAL_Delay(10);
    HAL_SPI_Transmit(&hspi2,spiTXbuf,5,10);
    HAL_Delay(10);
    cs_reset_2();// hight
}

void spi_tr_m3 (void)
{
    cs_set_3();//low
    HAL_Delay(10);
    HAL_SPI_Transmit(&hspi2,spiTXbuf,5,10);
    HAL_Delay(10);
    cs_reset_3();// hight
}

void spi_tr_m4 (void)
{
    cs_set_4();//low
    HAL_Delay(10);
    HAL_SPI_Transmit(&hspi2,spiTXbuf,5,10);
    HAL_Delay(10);
    cs_reset_4();// hight
}
void init_drive(void)
{
    ////////////////////GCONF///////////////////////////////////////
    spiTXbuf[0]=0x80;
    spiTXbuf[1]=0x00;
    spiTXbuf[2]=0x00;
    spiTXbuf[3]=0x00;
    spiTXbuf[4]=0x03;
    
    spi_tr_m1();
    HAL_Delay(50);
    spi_tr_m2();
    HAL_Delay(50);
    spi_tr_m3();
    HAL_Delay(50);
    spi_tr_m4();  


    //////////////////////CHOPCONF//////////////////////////////////////
    spiTXbuf[0]=0xEC;//ec
    spiTXbuf[1]=0x03;// microstep
    spiTXbuf[2]=0x01;
    spiTXbuf[3]=0x00;
    spiTXbuf[4]=0xC3;
    
    spi_tr_m1();
    HAL_Delay(50);
    spi_tr_m2();
    HAL_Delay(50);
    spi_tr_m3();
    HAL_Delay(50);
    spi_tr_m4();   


    ///////////////////////IHOLD_IRUN////////////////////////////////////////
    spiTXbuf[0]=0xB0;//b0
    spiTXbuf[1]=0x00;
    spiTXbuf[2]=0x06;//06 cerrent run   
    spiTXbuf[3]=0x1f;
    spiTXbuf[4]=0x05;//05//current stop
    
    spi_tr_m1();
    HAL_Delay(50);
    spi_tr_m2();
    HAL_Delay(50);
    spi_tr_m3();
    HAL_Delay(50);
    spi_tr_m4();    


   
}

void deinit_drive(void)
{
     ////////////////////GCONF///////////////////////////////////////
    spiTXbuf[0]=0x80;
    spiTXbuf[1]=0x00;
    spiTXbuf[2]=0x00;
    spiTXbuf[3]=0x00;
    spiTXbuf[4]=0x00;
    
    spi_tr_m1();
    HAL_Delay(50);
    spi_tr_m2();
    HAL_Delay(50);
    spi_tr_m3();
    HAL_Delay(50);
    spi_tr_m4();  
    
    //////////////////////CHOPCONF//////////////////////////////////////
    spiTXbuf[0]=0xEC;//ec
    spiTXbuf[1]=0x00;
    spiTXbuf[2]=0x00;
    spiTXbuf[3]=0x00;
    spiTXbuf[4]=0xC0;//c0
    
    spi_tr_m1();
    HAL_Delay(50);
    spi_tr_m2();
    HAL_Delay(50);
    spi_tr_m3();
    HAL_Delay(50);
    spi_tr_m4();   
    
    ///////////////////////IHOLD_IRUN////////////////////////////////////////
    spiTXbuf[0]=0xB0;//b0
    spiTXbuf[1]=0x00;
    spiTXbuf[2]=0x00;
    spiTXbuf[3]=0x00;
    spiTXbuf[4]=0x00;
    
    spi_tr_m1();
    HAL_Delay(50);
    spi_tr_m2();
    HAL_Delay(50);
    spi_tr_m3();
    HAL_Delay(50);
    spi_tr_m4();  
    
}

int mod(int16_t i)
{ 
	int16_t n=0;
	n=(~i)+1;
	return n;
}


int koordinate_motor(int8_t B0, uint8_t B1)
 {
	int64_t koordinate = 0;
	koordinate = (int64_t)B0*256 + B1;
	return koordinate;	 
 }

 int calc_end_step(int64_t data, int64_t coefficient)
 {
	int64_t end_step = 0;
	end_step = (data*coefficient);
	return end_step;	 
 }



void Home(void)
{
    if(RxHeader.StdId==ID_DISCR)
      {
          
          
          
          if((flag_Din1==HI)&&(flag_Din2==HI)/*&&(flag_Din3==HI)&&(flag_Din4==HI)*/)
          {
            
           /*  obnulit max znachenia
            pHeader.StdId=0x4A0;
            pHeader.RTR=CAN_RTR_DATA;
            pHeader.IDE=CAN_ID_STD;
            pHeader.DLC=8;
            pHeader.TransmitGlobalTime=DISABLE;
            HAL_CAN_AddTxMessage(&hcan,&pHeader,CAN_Rxdata_save_Din,&TxMailbox);
            */
           /* for(int n=0; n<=8; n++)
            {
               CAN_Rxdata_save_Din[n]=0;
            }*/
          }
          
      }
}

void koord(void) //rashot smeschenia napravlenia
{
    
					 
 //---raschot koordinaty X1-------------------------------------------------------------------------------
     if (data_x1<0)
		 {
			 if (x_1>0)
				 {
					 offset_x1 = x_1 + mod(data_x1); // raschot smechenia
					 HAL_GPIO_WritePin(DIR1_GPIO_Port,DIR1_Pin,GPIO_PIN_RESET);					 
			   }
			 if (x_1<0)
				 {
					 if (x_1>data_x1)
					 {
						offset_x1= x_1 + mod(data_x1);
						HAL_GPIO_WritePin(DIR1_GPIO_Port,DIR1_Pin,GPIO_PIN_RESET); 
					 }
					 
					 if (x_1<data_x1)
					 {
						offset_x1= mod (x_1) + data_x1;
						HAL_GPIO_WritePin(DIR1_GPIO_Port,DIR1_Pin,GPIO_PIN_SET); 
					 }
                 }
             if(x_1==0)
             {
                offset_x1=mod(data_x1);
                HAL_GPIO_WritePin(DIR1_GPIO_Port,DIR1_Pin,GPIO_PIN_RESET);
                
             }
		 }
		 
		 if (data_x1>0)
		 {
			 if (x_1<0)
				 {
					 offset_x1= mod (x_1) + data_x1;
					 HAL_GPIO_WritePin(DIR1_GPIO_Port,DIR1_Pin,GPIO_PIN_SET);					 
			   }
			 if (x_1>0)
				 {
					 if (x_1>data_x1)
					 {
						offset_x1= x_1 + mod(data_x1);
						HAL_GPIO_WritePin(DIR1_GPIO_Port,DIR1_Pin,GPIO_PIN_RESET); 
					 }
					 
					 if (x_1<data_x1)
					 {
						offset_x1= mod (x_1) + data_x1;
						HAL_GPIO_WritePin(DIR1_GPIO_Port,DIR1_Pin,GPIO_PIN_SET); 
					 }
                 }
             if(x_1==0)
             {
                offset_x1=data_x1;
                HAL_GPIO_WritePin(DIR1_GPIO_Port,DIR1_Pin,GPIO_PIN_SET);
                
             }       
		 }
		 
		 if (data_x1==0)
		 {
			 if (x_1<0)
				 {
					offset_x1=mod(x_1);
					HAL_GPIO_WritePin(DIR1_GPIO_Port,DIR1_Pin,GPIO_PIN_SET);					 
			   }
			 if (x_1>0)
				 {					 
					offset_x1= x_1;
					HAL_GPIO_WritePin(DIR1_GPIO_Port,DIR1_Pin,GPIO_PIN_RESET); 					 										 
			   }	   		 
		 } 
		 
		 		 
 //-------------------------------------------------------------------------------------------------------	 

 //---raschot koordinaty X2-------------------------------------------------------------------------------
     if (data_x2<0)
		 {
			 if (x_2>0)
				 {
					 offset_x2 = x_2 + mod(data_x2); // raschot smechenia
					 HAL_GPIO_WritePin(DIR2_GPIO_Port,DIR2_Pin,GPIO_PIN_SET);					 
			   }
			 if (x_2<0)
				 {
					 if (x_2>data_x2)
					 {
						offset_x2= x_2 + mod(data_x2);
						HAL_GPIO_WritePin(DIR2_GPIO_Port,DIR2_Pin,GPIO_PIN_SET); 
					 }
					 
					 if (x_2<data_x2)
					 {
						offset_x2= mod(x_2) + data_x2;
						HAL_GPIO_WritePin(DIR2_GPIO_Port,DIR2_Pin,GPIO_PIN_RESET); 
					 }
			   }
             if(x_2==0)
             {
                offset_x2=mod(data_x2);
                HAL_GPIO_WritePin(DIR2_GPIO_Port,DIR2_Pin,GPIO_PIN_SET);
                
             }
		 }
		 
		 if (data_x2>0)
		 {
			 if (x_2<0)
				 {
					 offset_x2= mod (x_2) + data_x2;
					 HAL_GPIO_WritePin(DIR2_GPIO_Port,DIR2_Pin,GPIO_PIN_RESET);					 
			   }
			 if (x_2>0)
				 {
					 if (x_2>data_x2)
					 {
						offset_x2= x_2 + mod(data_x2);
						HAL_GPIO_WritePin(DIR2_GPIO_Port,DIR2_Pin,GPIO_PIN_SET); 
					 }
					 
					 if (x_2<data_x2)
					 {
						offset_x2= mod (x_2) + data_x2;
						HAL_GPIO_WritePin(DIR2_GPIO_Port,DIR2_Pin,GPIO_PIN_RESET); 
					 }
			   }
             if(x_2==0)
             {
                offset_x2=data_x2;
                HAL_GPIO_WritePin(DIR2_GPIO_Port,DIR2_Pin,GPIO_PIN_RESET);
                
             }           
		 }
		 
		 if (data_x2==0)
		 {
			 if (x_2<0)
				 {
					offset_x2= mod(x_2);
					HAL_GPIO_WritePin(DIR2_GPIO_Port,DIR2_Pin,GPIO_PIN_RESET);					 
			   }
			 if (x_2>0)
				 {					 
					offset_x2= x_2;
					HAL_GPIO_WritePin(DIR2_GPIO_Port,DIR2_Pin,GPIO_PIN_SET); 					 										 
			   }	



				 
		 } 
		 
		 
//-------------------------------------------------------------------------------------------------------	 
		 
//---raschot koordinaty Y1-------------------------------------------------------------------------------		 
		 if(data_y1>0)
		 {
			 if(data_y1<y_1)
					 {
							 offset_y1=y_1-data_y1;
							 HAL_GPIO_WritePin(DIR3_GPIO_Port,DIR3_Pin,GPIO_PIN_SET);
							 
					 }
					else
					 {
							 offset_y1=data_y1-y_1;
							 HAL_GPIO_WritePin(DIR3_GPIO_Port,DIR3_Pin,GPIO_PIN_RESET);
					 }
			 }

//-------------------------------------------------------------------------------------------------------

//---raschot koordinaty Y2-------------------------------------------------------------------------------		 
		 if(data_y2>0)
		 {
			 if(data_y2<y_2)
         {
             offset_y2=y_2-data_y1;
             HAL_GPIO_WritePin(DIR4_GPIO_Port,DIR4_Pin,GPIO_PIN_SET);
             
         }
         else
         {
             offset_y2=data_y2-y_2;
            HAL_GPIO_WritePin(DIR4_GPIO_Port,DIR4_Pin,GPIO_PIN_RESET);
         }
			 }
//-------------------------------------------------------------------------------------------------------		 

		 
            step_1_n_end=calc_end_step(offset_x1,N_STEP_MM_OX);
            HAL_Delay(0);
         	step_2_n_end=calc_end_step(offset_x2,N_STEP_MM_OX);
            HAL_Delay(0);
            step_3_n_end=calc_end_step(offset_y1,N_STEP_MM_OY);
            HAL_Delay(0);
            step_4_n_end=calc_end_step(offset_y2,N_STEP_MM_OY);
           
           
			if(step_1_n_end>0)
            {
                HAL_TIM_Base_Start_IT(&htim1);    						    
            }               
			 
            if(step_2_n_end>0)
            {
                HAL_TIM_Base_Start_IT(&htim2);    						    
            } 

            if(step_3_n_end>0)
            {
                HAL_TIM_Base_Start_IT(&htim3);    						    
            } 
            
            if(step_4_n_end>0)
            {
                HAL_TIM_Base_Start_IT(&htim4);    						    
            } 

}
//////////////////////////////////////////////////////////////////////////////
int8_t flag_error=0;
void parser (void) //pereschot koordinat
{
    int8_t flag_piema=0;
    if(RxHeader.StdId==ID_MOTOR)
      {
          
          
           for(int n=0; n<=8; n++)
           {
               CAN_Rxdata_save_kord[n]=CAN_Rxdata[n];
           }
                                  
           
					 
					                     
           data_x1=koordinate_motor(CAN_Rxdata_save_kord[0],CAN_Rxdata_save_kord[1]);
           HAL_Delay(0);
           data_x2=koordinate_motor(CAN_Rxdata_save_kord[4],CAN_Rxdata_save_kord[5]);
           HAL_Delay(0);
           data_y1=koordinate_motor(CAN_Rxdata_save_kord[2],CAN_Rxdata_save_kord[3]);
           HAL_Delay(0);
           data_y2=koordinate_motor(CAN_Rxdata_save_kord[6],CAN_Rxdata_save_kord[7]);

			RxHeader.StdId=0x00;
            flag_piema=1;
        }
        
			if (data_x1==data_x2)
			{
                pHeader.StdId=0x4FF;
                pHeader.RTR=CAN_RTR_DATA;
                pHeader.IDE=CAN_ID_STD;
                pHeader.DLC=8;
                pHeader.TransmitGlobalTime=DISABLE;  
                
                CAN_Txdata[0]=0;
                CAN_Txdata[1]=0;
                CAN_Txdata[2]=0;
                CAN_Txdata[3]=0;
                CAN_Txdata[4]=0;
                CAN_Txdata[5]=0;
                CAN_Txdata[6]=0;
                CAN_Txdata[7]=1;
                if(flag_error==0)
                {
                    HAL_CAN_AddTxMessage(&hcan,&pHeader,CAN_Txdata,&TxMailbox);
                    flag_error=1;
                }
                
                flag_piema=0;
            }	
			if ((data_x1<(data_x2+RASSTOIANIE_MEGHDY_LUCHAMI))&&(data_x2>(data_x1+RASSTOIANIE_MEGHDY_LUCHAMI))&&(flag_piema==1))
			{
                pHeader.StdId=0x4FF;
                pHeader.RTR=CAN_RTR_DATA;
                pHeader.IDE=CAN_ID_STD;
                pHeader.DLC=8;
                pHeader.TransmitGlobalTime=DISABLE;  
                
                CAN_Txdata[0]=0;
                CAN_Txdata[1]=0;
                CAN_Txdata[2]=0;
                CAN_Txdata[3]=0;
                CAN_Txdata[4]=0;
                CAN_Txdata[5]=0;
                CAN_Txdata[6]=0;
                CAN_Txdata[7]=1;
                if(flag_error==0)
                {
                    HAL_CAN_AddTxMessage(&hcan,&pHeader,CAN_Txdata,&TxMailbox);
                    flag_error=1;
                }
                
                flag_piema=0;
            }
            
            if ((data_x1<(x_2+RASSTOIANIE_MEGHDY_LUCHAMI))&&(data_x2>(x_1+RASSTOIANIE_MEGHDY_LUCHAMI))&&(flag_piema==1))
			{
                pHeader.StdId=0x4FF;
                pHeader.RTR=CAN_RTR_DATA;
                pHeader.IDE=CAN_ID_STD;
                pHeader.DLC=8;
                pHeader.TransmitGlobalTime=DISABLE;  
                
                CAN_Txdata[0]=0;
                CAN_Txdata[1]=0;
                CAN_Txdata[2]=0;
                CAN_Txdata[3]=0;
                CAN_Txdata[4]=0;
                CAN_Txdata[5]=0;
                CAN_Txdata[6]=0;
                CAN_Txdata[7]=1;
                if(flag_error==0)
                {
                    HAL_CAN_AddTxMessage(&hcan,&pHeader,CAN_Txdata,&TxMailbox);
                    flag_error=1;
                }
                
                flag_piema=0;
            }
				
				if(data_x1==0x7FFF)
                {
                    data_x1=x_1;
                }
                if(data_x2==0x7FFF)
                {
                    data_x2=x_2;
                }
                if(data_y1==0x7FFF)
                {
                    data_y1=y_1;
                }
                if(data_y2==0x7FFF)
                {
                    data_y2=y_2;
                }
				
                if (data_x1>x_1_max)
				{
					
                    
                    pHeader.StdId=0x4FF;
                    pHeader.RTR=CAN_RTR_DATA;
                    pHeader.IDE=CAN_ID_STD;
                    pHeader.DLC=8;
                    pHeader.TransmitGlobalTime=DISABLE;  
                
                    CAN_Txdata[0]=0;
                    CAN_Txdata[1]=0;
                    CAN_Txdata[2]=0;
                    CAN_Txdata[3]=0;
                    CAN_Txdata[4]=0;
                    CAN_Txdata[5]=0;
                    CAN_Txdata[6]=0;
                    CAN_Txdata[7]=2;
                   // if(flag_error==0)
                   // {
                        HAL_CAN_AddTxMessage(&hcan,&pHeader,CAN_Txdata,&TxMailbox);
                       // flag_error=1;
                    //}
				}
               
                
                if (data_x2<x_2_max)
				{
					data_x2=x_1_max;
                    pHeader.StdId=0x4FF;
                    pHeader.RTR=CAN_RTR_DATA;
                    pHeader.IDE=CAN_ID_STD;
                    pHeader.DLC=8;
                    pHeader.TransmitGlobalTime=DISABLE;  
                
                    CAN_Txdata[0]=0;
                    CAN_Txdata[1]=0;
                    CAN_Txdata[2]=0;
                    CAN_Txdata[3]=0;
                    CAN_Txdata[4]=0;
                    CAN_Txdata[5]=0;
                    CAN_Txdata[6]=0;
                    CAN_Txdata[7]=3;
                   // if(flag_error==0)
                   // {
                        HAL_CAN_AddTxMessage(&hcan,&pHeader,CAN_Txdata,&TxMailbox);
                       // flag_error=1;
                    //}
				}
                if(data_y1>y_1_max)
                {
                    data_y1=y_1_max;
                    
                    pHeader.StdId=0x4FF;
                    pHeader.RTR=CAN_RTR_DATA;
                    pHeader.IDE=CAN_ID_STD;
                    pHeader.DLC=8;
                    pHeader.TransmitGlobalTime=DISABLE;  
                
                    CAN_Txdata[0]=0;
                    CAN_Txdata[1]=0;
                    CAN_Txdata[2]=0;
                    CAN_Txdata[3]=0;
                    CAN_Txdata[4]=0;
                    CAN_Txdata[5]=0;
                    CAN_Txdata[6]=0;
                    CAN_Txdata[7]=6;
                   // if(flag_error==0)
                   // {
                        HAL_CAN_AddTxMessage(&hcan,&pHeader,CAN_Txdata,&TxMailbox);
                       // flag_error=1;
                    //}
                }
                if(data_y2>y_2_max)
                {
                    data_y2=y_2_max;
                    
                    pHeader.StdId=0x4FF;
                    pHeader.RTR=CAN_RTR_DATA;
                    pHeader.IDE=CAN_ID_STD;
                    pHeader.DLC=8;
                    pHeader.TransmitGlobalTime=DISABLE;  
                
                    CAN_Txdata[0]=0;
                    CAN_Txdata[1]=0;
                    CAN_Txdata[2]=0;
                    CAN_Txdata[3]=0;
                    CAN_Txdata[4]=0;
                    CAN_Txdata[5]=0;
                    CAN_Txdata[6]=0;
                    CAN_Txdata[7]=7;
                   // if(flag_error==0)
                   // {
                        HAL_CAN_AddTxMessage(&hcan,&pHeader,CAN_Txdata,&TxMailbox);
                       // flag_error=1;
                    //}
                }
                if(data_y1<y_1_min)
                {
                    data_y1=y_1_min;
                    
                    pHeader.StdId=0x4FF;
                    pHeader.RTR=CAN_RTR_DATA;
                    pHeader.IDE=CAN_ID_STD;
                    pHeader.DLC=8;
                    pHeader.TransmitGlobalTime=DISABLE;  
                
                    CAN_Txdata[0]=0;
                    CAN_Txdata[1]=0;
                    CAN_Txdata[2]=0;
                    CAN_Txdata[3]=0;
                    CAN_Txdata[4]=0;
                    CAN_Txdata[5]=0;
                    CAN_Txdata[6]=0;
                    CAN_Txdata[7]=8;
                   // if(flag_error==0)
                   // {
                        HAL_CAN_AddTxMessage(&hcan,&pHeader,CAN_Txdata,&TxMailbox);
                       // flag_error=1;
                    //}
                }
                if(data_y2<y_2_min)
                {
                    data_y2=y_2_min;
                    pHeader.StdId=0x4FF;
                    pHeader.RTR=CAN_RTR_DATA;
                    pHeader.IDE=CAN_ID_STD;
                    pHeader.DLC=8;
                    pHeader.TransmitGlobalTime=DISABLE;  
                
                    CAN_Txdata[0]=0;
                    CAN_Txdata[1]=0;
                    CAN_Txdata[2]=0;
                    CAN_Txdata[3]=0;
                    CAN_Txdata[4]=0;
                    CAN_Txdata[5]=0;
                    CAN_Txdata[6]=0;
                    CAN_Txdata[7]=9;
                   // if(flag_error==0)
                   // {
                        HAL_CAN_AddTxMessage(&hcan,&pHeader,CAN_Txdata,&TxMailbox);
                       // flag_error=1;
                    //}
                }
                
		if(flag_piema!=0)
       {
            koord();
            flag_error=0;
       }
    
}    
        
//--------------------------------------------------------------------------------------------------------------------------------------      
 
         
 
         
 void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim) //prerivanie tamerov
{
        if(htim->Instance == TIM1) //check if the interrupt comes from TIM1
        {
            if(step_1_count<step_1_n_end)
            {
                HAL_GPIO_TogglePin(STEP1_GPIO_Port,STEP1_Pin);
                step_1_count++;
                step_1_flag=LOW;
			
            } 
            else
            {       
                x_1= data_x1;
                step_1_flag=HI;
                HAL_TIM_Base_Stop_IT(&htim1);
			  
            }
          }
        if(htim->Instance == TIM2) //check if the interrupt comes from TIM2
        {
            if(step_2_count<step_2_n_end)
            {
                HAL_GPIO_TogglePin(STEP2_GPIO_Port,STEP2_Pin);
                step_2_count++;
                step_2_flag=LOW;
            }
            else
            {
                x_2=data_x2;			
                step_2_flag=HI;
                HAL_TIM_Base_Stop_IT(&htim2); 
			
            }
        }
        if(htim->Instance == TIM3) //check if the interrupt comes from TIM3
        {
            if(step_3_count<=step_3_n_end)
            {
                HAL_GPIO_TogglePin(STEP3_GPIO_Port,STEP3_Pin);
                step_3_count++;
                step_3_flag=LOW;
            }
            if(step_3_count>step_3_n_end)
            {
                y_1=data_y1;
                step_3_flag=HI;
                HAL_TIM_Base_Stop_IT(&htim3);
            }
        }
        if(htim->Instance == TIM4) //check if the interrupt comes from TIM4
        {
            if(step_4_count<=step_4_n_end)
            {
                HAL_GPIO_TogglePin(STEP4_GPIO_Port,STEP4_Pin);
                step_4_count++;
                step_4_flag=LOW;
            }
            if(step_4_count>step_4_n_end)
            {
                y_2=data_y2;
                step_4_flag=HI;
                HAL_TIM_Base_Stop_IT(&htim4);
            }
        }
}          

int8_t flag_message=0;
void koord_flag (void)//proverka flagov obnulenie peremennih
{
  
  if((step_1_flag==HI)&&(step_1_count!=0))     
    {

        offset_x1 = 0;
        step_1_count = 0;                               
        step_1_n_end=0;
		flag_message=1;
			          
    }
    
    if((step_2_flag==HI)&&(step_2_count!=0))  
    {
        offset_x2 = 0;
        step_2_count = 0;                           
        step_2_n_end=0;
        flag_message=1;
    }
    
		
	if((step_3_flag==HI)&&(step_3_count!=0))  
    {
        offset_y1 = 0;
        step_3_count = 0;                           
        step_3_n_end=0;
        flag_message=1;
    }
    
		
	if((step_4_flag==HI)&&(step_4_count!=0))  
    {
        offset_y2 = 0;
        step_4_count = 0;                           
        step_4_n_end=0;
        flag_message=1;
    }
    
    
	 if((step_1_flag==HI)&&(step_2_flag==HI)&&(step_3_flag==HI)&&(step_4_flag==HI)&&(flag_message==1))
     {
		pHeader.StdId=ID_MOTOR_ANSVER;
        pHeader.RTR=CAN_RTR_DATA;
        pHeader.IDE=CAN_ID_STD;
        pHeader.DLC=8;
        pHeader.TransmitGlobalTime=DISABLE;
        HAL_CAN_AddTxMessage(&hcan,&pHeader,CAN_Rxdata_save_kord,&TxMailbox);  
		flag_message=0;
     }
		 if((step_1_count>0)||(step_2_count>0)||(step_3_count>0)||(step_4_count>0))
		 {
			if((step_1_flag==LOW)||(step_2_flag==LOW)||(step_3_flag==LOW)||(step_4_flag==LOW))
			{
					pHeader.StdId=ID_MOTOR_ANSVER;
					pHeader.RTR=CAN_RTR_DATA;
					pHeader.IDE=CAN_ID_STD;
					pHeader.DLC=0;
					pHeader.TransmitGlobalTime=DISABLE;
                    HAL_Delay(100);
					HAL_CAN_AddTxMessage(&hcan,&pHeader,CAN_Rxdata_save_kord,&TxMailbox);
			 }
		 }
    
 }   

 void HAL_CAN_RxFifo0MsgPendingCallback(CAN_HandleTypeDef *hcan)//priem soobchenia v prerivanii
   {
      if(HAL_CAN_GetRxMessage(hcan,CAN_RX_FIFO0,&RxHeader,CAN_Rxdata) !=HAL_OK)
		{
            Error_Handler();
		} 
     if(RxHeader.StdId==ID_DISCR)
     {         
        if(CAN_Rxdata[0]==0xFF)
        {
            HAL_TIM_Base_Stop_IT(&htim1);
            flag_Din1=HI;
        }
        else
        {
            flag_Din1=LOW;
        }
        if(CAN_Rxdata[1]==0xFF)
        {
            HAL_TIM_Base_Stop_IT(&htim2);
            flag_Din2=HI;
        }
        else
        {
            flag_Din2=LOW;
        }
        if(CAN_Rxdata[2]==0xFF)
        {
            HAL_TIM_Base_Stop_IT(&htim3);
            flag_Din3=HI;
        }
        else
        {
            flag_Din3=LOW;
        }
        if(CAN_Rxdata[3]==0xFF)
        {
            HAL_TIM_Base_Stop_IT(&htim4);
            flag_Din4=HI;
        }
        else
        {
            flag_Din4=LOW;
        }
    }
}
 
/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */
    
  /* USER CODE END 1 */
  

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */
   // 
  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_CAN_Init();
  MX_SPI2_Init();
  MX_TIM4_Init();
  MX_USART1_UART_Init();
  MX_TIM1_Init();
  MX_TIM2_Init();
  MX_TIM3_Init();
  /* USER CODE BEGIN 2 */
    HAL_GPIO_WritePin(CS1_GPIO_Port, CS1_Pin, GPIO_PIN_SET);
    HAL_GPIO_WritePin(CS2_GPIO_Port, CS2_Pin, GPIO_PIN_SET);
    HAL_GPIO_WritePin(CS3_GPIO_Port, CS3_Pin, GPIO_PIN_SET);
    HAL_GPIO_WritePin(CS4_GPIO_Port, CS4_Pin, GPIO_PIN_SET);
    init_drive();
    HAL_CAN_Start(&hcan); //start CAN
    if(HAL_CAN_ActivateNotification(&hcan, CAN_IT_RX_FIFO0_MSG_PENDING)!=HAL_OK)
    {
      Error_Handler();  
    }
    HAL_GPIO_WritePin(DIR1_GPIO_Port,DIR1_Pin,GPIO_PIN_SET);
    HAL_GPIO_WritePin(DIR2_GPIO_Port,DIR2_Pin,GPIO_PIN_SET);
    HAL_GPIO_WritePin(DIR3_GPIO_Port,DIR3_Pin,GPIO_PIN_SET);
    HAL_GPIO_WritePin(DIR4_GPIO_Port,DIR4_Pin,GPIO_PIN_SET);
    x_1 = x_1_max;
	x_2 = x_2_max;
	y_1 = y_1_max;
	y_2 = y_2_max;
    
   
   
  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {  
    Home(); 
     // koord();
    parser();
	//	Home(); 
	koord_flag();
 
    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

  /** Initializes the CPU, AHB and APB busses clocks 
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_ON;
  RCC_OscInitStruct.HSEPredivValue = RCC_HSE_PREDIV_DIV1;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLMUL = RCC_PLL_MUL8;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB busses clocks 
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV2;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_2) != HAL_OK)
  {
    Error_Handler();
  }
}

/**
  * @brief CAN Initialization Function
  * @param None
  * @retval None
  */
static void MX_CAN_Init(void)
{

  /* USER CODE BEGIN CAN_Init 0 */

  /* USER CODE END CAN_Init 0 */

  /* USER CODE BEGIN CAN_Init 1 */

  /* USER CODE END CAN_Init 1 */
  hcan.Instance = CAN1;
  hcan.Init.Prescaler = 4;
  hcan.Init.Mode = CAN_MODE_NORMAL;
  hcan.Init.SyncJumpWidth = CAN_SJW_2TQ;
  hcan.Init.TimeSeg1 = CAN_BS1_13TQ;
  hcan.Init.TimeSeg2 = CAN_BS2_2TQ;
  hcan.Init.TimeTriggeredMode = DISABLE;
  hcan.Init.AutoBusOff = DISABLE;
  hcan.Init.AutoWakeUp = DISABLE;
  hcan.Init.AutoRetransmission = DISABLE;
  hcan.Init.ReceiveFifoLocked = DISABLE;
  hcan.Init.TransmitFifoPriority = DISABLE;
  if (HAL_CAN_Init(&hcan) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN CAN_Init 2 */
    sFilterConfig.FilterBank=0;
    sFilterConfig.FilterMode=CAN_FILTERMODE_IDLIST;
    sFilterConfig.FilterScale=CAN_FILTERSCALE_32BIT;
    sFilterConfig.FilterIdHigh=ID_MOTOR<<5; 
	sFilterConfig.FilterIdLow=0x00;
	sFilterConfig.FilterMaskIdHigh=0xA0<<5;
	sFilterConfig.FilterMaskIdLow=0x00;
	sFilterConfig.FilterFIFOAssignment=CAN_RX_FIFO0;
	sFilterConfig.FilterActivation=ENABLE;
    
    HAL_CAN_ConfigFilter(&hcan, &sFilterConfig);
  
    sFilterConfig.FilterBank=1;
    sFilterConfig.FilterMode=CAN_FILTERMODE_IDLIST;
    sFilterConfig.FilterScale=CAN_FILTERSCALE_32BIT;
    sFilterConfig.FilterIdHigh=ID_DISCR<<5; 
	sFilterConfig.FilterIdLow=0x00;
	sFilterConfig.FilterMaskIdHigh=0x30<<5;
	sFilterConfig.FilterMaskIdLow=0x00;
	sFilterConfig.FilterFIFOAssignment=CAN_RX_FIFO0;
	sFilterConfig.FilterActivation=ENABLE;
  
	HAL_CAN_ConfigFilter(&hcan, &sFilterConfig);
    
   
   // HAL_CAN_ActivateNotification(&hcan, CAN_IT_RX_FIFO0_MSG_PENDING | CAN_IT_TX_MAILBOX_EMPTY);
   // HAL_CAN_ActivateNotification(&hcan, CAN_IT_TX_MAILBOX_EMPTY);
    
  /* USER CODE END CAN_Init 2 */

}

/**
  * @brief SPI2 Initialization Function
  * @param None
  * @retval None
  */
static void MX_SPI2_Init(void)
{

  /* USER CODE BEGIN SPI2_Init 0 */

  /* USER CODE END SPI2_Init 0 */

  /* USER CODE BEGIN SPI2_Init 1 */

  /* USER CODE END SPI2_Init 1 */
  /* SPI2 parameter configuration*/
  hspi2.Instance = SPI2;
  hspi2.Init.Mode = SPI_MODE_MASTER;
  hspi2.Init.Direction = SPI_DIRECTION_2LINES;
  hspi2.Init.DataSize = SPI_DATASIZE_8BIT;
  hspi2.Init.CLKPolarity = SPI_POLARITY_HIGH;
  hspi2.Init.CLKPhase = SPI_PHASE_2EDGE;
  hspi2.Init.NSS = SPI_NSS_SOFT;
  hspi2.Init.BaudRatePrescaler = SPI_BAUDRATEPRESCALER_16;
  hspi2.Init.FirstBit = SPI_FIRSTBIT_MSB;
  hspi2.Init.TIMode = SPI_TIMODE_DISABLE;
  hspi2.Init.CRCCalculation = SPI_CRCCALCULATION_DISABLE;
  hspi2.Init.CRCPolynomial = 10;
  if (HAL_SPI_Init(&hspi2) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN SPI2_Init 2 */

  /* USER CODE END SPI2_Init 2 */

}

/**
  * @brief TIM1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM1_Init(void)
{

  /* USER CODE BEGIN TIM1_Init 0 */

  /* USER CODE END TIM1_Init 0 */

  TIM_ClockConfigTypeDef sClockSourceConfig = {0};
  TIM_MasterConfigTypeDef sMasterConfig = {0};

  /* USER CODE BEGIN TIM1_Init 1 */

  /* USER CODE END TIM1_Init 1 */
  htim1.Instance = TIM1;
  htim1.Init.Prescaler = 10;
  htim1.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim1.Init.Period = 500;
  htim1.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim1.Init.RepetitionCounter = 0;
  htim1.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_Base_Init(&htim1) != HAL_OK)
  {
    Error_Handler();
  }
  sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
  if (HAL_TIM_ConfigClockSource(&htim1, &sClockSourceConfig) != HAL_OK)
  {
    Error_Handler();
  }
  sMasterConfig.MasterOutputTrigger = TIM_TRGO_UPDATE;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim1, &sMasterConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM1_Init 2 */

  /* USER CODE END TIM1_Init 2 */

}

/**
  * @brief TIM2 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM2_Init(void)
{

  /* USER CODE BEGIN TIM2_Init 0 */

  /* USER CODE END TIM2_Init 0 */

  TIM_ClockConfigTypeDef sClockSourceConfig = {0};
  TIM_MasterConfigTypeDef sMasterConfig = {0};

  /* USER CODE BEGIN TIM2_Init 1 */

  /* USER CODE END TIM2_Init 1 */
  htim2.Instance = TIM2;
  htim2.Init.Prescaler = 10;
  htim2.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim2.Init.Period = 500;
  htim2.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim2.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_Base_Init(&htim2) != HAL_OK)
  {
    Error_Handler();
  }
  sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
  if (HAL_TIM_ConfigClockSource(&htim2, &sClockSourceConfig) != HAL_OK)
  {
    Error_Handler();
  }
  sMasterConfig.MasterOutputTrigger = TIM_TRGO_UPDATE;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim2, &sMasterConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM2_Init 2 */

  /* USER CODE END TIM2_Init 2 */

}

/**
  * @brief TIM3 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM3_Init(void)
{

  /* USER CODE BEGIN TIM3_Init 0 */

  /* USER CODE END TIM3_Init 0 */

  TIM_ClockConfigTypeDef sClockSourceConfig = {0};
  TIM_MasterConfigTypeDef sMasterConfig = {0};

  /* USER CODE BEGIN TIM3_Init 1 */

  /* USER CODE END TIM3_Init 1 */
  htim3.Instance = TIM3;
  htim3.Init.Prescaler = 10;
  htim3.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim3.Init.Period = 500;
  htim3.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim3.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_Base_Init(&htim3) != HAL_OK)
  {
    Error_Handler();
  }
  sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
  if (HAL_TIM_ConfigClockSource(&htim3, &sClockSourceConfig) != HAL_OK)
  {
    Error_Handler();
  }
  sMasterConfig.MasterOutputTrigger = TIM_TRGO_UPDATE;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim3, &sMasterConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM3_Init 2 */

  /* USER CODE END TIM3_Init 2 */

}

/**
  * @brief TIM4 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM4_Init(void)
{

  /* USER CODE BEGIN TIM4_Init 0 */

  /* USER CODE END TIM4_Init 0 */

  TIM_ClockConfigTypeDef sClockSourceConfig = {0};
  TIM_MasterConfigTypeDef sMasterConfig = {0};

  /* USER CODE BEGIN TIM4_Init 1 */

  /* USER CODE END TIM4_Init 1 */
  htim4.Instance = TIM4;
  htim4.Init.Prescaler = 10;
  htim4.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim4.Init.Period = 500;
  htim4.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim4.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_Base_Init(&htim4) != HAL_OK)
  {
    Error_Handler();
  }
  sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
  if (HAL_TIM_ConfigClockSource(&htim4, &sClockSourceConfig) != HAL_OK)
  {
    Error_Handler();
  }
  sMasterConfig.MasterOutputTrigger = TIM_TRGO_UPDATE;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim4, &sMasterConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM4_Init 2 */

  /* USER CODE END TIM4_Init 2 */

}

/**
  * @brief USART1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_USART1_UART_Init(void)
{

  /* USER CODE BEGIN USART1_Init 0 */

  /* USER CODE END USART1_Init 0 */

  /* USER CODE BEGIN USART1_Init 1 */

  /* USER CODE END USART1_Init 1 */
  huart1.Instance = USART1;
  huart1.Init.BaudRate = 115200;
  huart1.Init.WordLength = UART_WORDLENGTH_8B;
  huart1.Init.StopBits = UART_STOPBITS_1;
  huart1.Init.Parity = UART_PARITY_NONE;
  huart1.Init.Mode = UART_MODE_TX_RX;
  huart1.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart1.Init.OverSampling = UART_OVERSAMPLING_16;
  if (HAL_UART_Init(&huart1) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN USART1_Init 2 */

  /* USER CODE END USART1_Init 2 */

}

/**
  * @brief GPIO Initialization Function
  * @param None
  * @retval None
  */
static void MX_GPIO_Init(void)
{
  GPIO_InitTypeDef GPIO_InitStruct = {0};

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOD_CLK_ENABLE();
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOA, DIR1_Pin|STEP1_Pin|CS1_Pin|DIR3_Pin 
                          |STEP3_Pin|RE_DE_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOB, CS3_Pin|DIR2_Pin|CS4_Pin|DIR4_Pin 
                          |STEP4_Pin|STEP2_Pin|CS2_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pins : DIR1_Pin STEP1_Pin CS1_Pin DIR3_Pin 
                           STEP3_Pin RE_DE_Pin */
  GPIO_InitStruct.Pin = DIR1_Pin|STEP1_Pin|CS1_Pin|DIR3_Pin 
                          |STEP3_Pin|RE_DE_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

  /*Configure GPIO pins : CS3_Pin DIR2_Pin CS4_Pin DIR4_Pin 
                           STEP4_Pin STEP2_Pin CS2_Pin */
  GPIO_InitStruct.Pin = CS3_Pin|DIR2_Pin|CS4_Pin|DIR4_Pin 
                          |STEP4_Pin|STEP2_Pin|CS2_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

}

/* USER CODE BEGIN 4 */

/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */

  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{ 
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
