/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2020 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32f1xx_hal.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */  
    extern uint16_t step_1_count; 
    extern uint16_t step_1_n_end; 
    extern uint8_t step_1_flag; 
    
    extern uint16_t step_2_count; 
    extern uint16_t step_2_n_end; 
    extern uint8_t step_2_flag; 
    
    extern uint16_t step_3_count; 
    extern uint16_t step_3_n_end; 
    extern uint8_t step_3_flag; 
    
    extern uint16_t step_4_count; 
    extern uint16_t step_4_n_end; 
    extern uint8_t step_4_flag; 
/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */

/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */

/* USER CODE END EM */

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */

/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define DIR1_Pin GPIO_PIN_3
#define DIR1_GPIO_Port GPIOA
#define STEP1_Pin GPIO_PIN_4
#define STEP1_GPIO_Port GPIOA
#define CS1_Pin GPIO_PIN_5
#define CS1_GPIO_Port GPIOA
#define DIR3_Pin GPIO_PIN_6
#define DIR3_GPIO_Port GPIOA
#define STEP3_Pin GPIO_PIN_7
#define STEP3_GPIO_Port GPIOA
#define CS3_Pin GPIO_PIN_0
#define CS3_GPIO_Port GPIOB
#define RE_DE_Pin GPIO_PIN_8
#define RE_DE_GPIO_Port GPIOA
#define DIR2_Pin GPIO_PIN_4
#define DIR2_GPIO_Port GPIOB
#define CS4_Pin GPIO_PIN_5
#define CS4_GPIO_Port GPIOB
#define DIR4_Pin GPIO_PIN_6
#define DIR4_GPIO_Port GPIOB
#define STEP4_Pin GPIO_PIN_7
#define STEP4_GPIO_Port GPIOB
#define STEP2_Pin GPIO_PIN_8
#define STEP2_GPIO_Port GPIOB
#define CS2_Pin GPIO_PIN_9
#define CS2_GPIO_Port GPIOB
/* USER CODE BEGIN Private defines */

/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
