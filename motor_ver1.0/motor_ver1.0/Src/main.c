/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2020 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "main.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
#define cs_set_1() HAL_GPIO_WritePin(CS1_GPIO_Port, CS1_Pin, GPIO_PIN_RESET)
#define cs_reset_1() HAL_GPIO_WritePin(CS1_GPIO_Port, CS1_Pin, GPIO_PIN_SET)


#define cs_set_2() HAL_GPIO_WritePin(CS2_GPIO_Port, CS2_Pin, GPIO_PIN_RESET)
#define cs_reset_2() HAL_GPIO_WritePin(CS2_GPIO_Port, CS2_Pin, GPIO_PIN_SET)


#define cs_set_3() HAL_GPIO_WritePin(CS3_GPIO_Port, CS3_Pin, GPIO_PIN_RESET)
#define cs_reset_3() HAL_GPIO_WritePin(CS3_GPIO_Port, CS3_Pin, GPIO_PIN_SET)


#define cs_set_4() HAL_GPIO_WritePin(CS4_GPIO_Port, CS4_Pin, GPIO_PIN_RESET)
#define cs_reset_4() HAL_GPIO_WritePin(CS4_GPIO_Port, CS4_Pin, GPIO_PIN_SET)


/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
CAN_HandleTypeDef hcan;

SPI_HandleTypeDef hspi2;

TIM_HandleTypeDef htim1;
TIM_HandleTypeDef htim2;
TIM_HandleTypeDef htim3;
TIM_HandleTypeDef htim4;

UART_HandleTypeDef huart1;

/* USER CODE BEGIN PV */
uint8_t spiTXbuf[5]={0};
char TXbuf[8]={0};
uint8_t RxData_CAN[8]={0};
uint8_t TxData_CAN[8]={0};
uint8_t spiRXbuf[5]={0};

unsigned int step_1=0;
unsigned int end_step_1=0;
unsigned int step_1_f=0;

unsigned int step_2=0;
unsigned int end_step_2=0;
unsigned int step_2_f=0;

unsigned int step_3=0;
unsigned int end_step_3=0;
unsigned int step_3_f=0;

unsigned int step_4=0;
unsigned int end_step_4=0;
unsigned int step_4_f=0;


volatile int D=0;
volatile int A=0;

int x_1=0;
int x_2=0;
int y_1=0;
int y_2=0;

CAN_FilterTypeDef sFilterConfig;
CAN_TxHeaderTypeDef pHeader;
CAN_RxHeaderTypeDef RxHeader;
uint8_t CAN_Txdata[8];
uint8_t CAN_Rxdata_kor[8];
uint8_t CAN_Rxdata[8];
uint32_t TxMailbox;
//CAN_TxMailBox_TypeDef TxMailbox;
//volatile int D=0;
//volatile int A=0;
/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_CAN_Init(void);
static void MX_SPI2_Init(void);
static void MX_TIM4_Init(void);
static void MX_USART1_UART_Init(void);
static void MX_TIM1_Init(void);
static void MX_TIM2_Init(void);
static void MX_TIM3_Init(void);
/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */
void spi_tr_m1 (void)
{
    cs_set_1();//low
    HAL_Delay(10);
    HAL_SPI_Transmit(&hspi2,spiTXbuf,5,10);
    HAL_Delay(10);
    cs_reset_1();// hight
}

void spi_tr_m2 (void)
{
    cs_set_2();//low
    HAL_Delay(10);
    HAL_SPI_Transmit(&hspi2,spiTXbuf,5,10);
    HAL_Delay(10);
    cs_reset_2();// hight
}

void spi_tr_m3 (void)
{
    cs_set_3();//low
    HAL_Delay(10);
    HAL_SPI_Transmit(&hspi2,spiTXbuf,5,10);
    HAL_Delay(10);
    cs_reset_3();// hight
}

void spi_tr_m4 (void)
{
    cs_set_4();//low
    HAL_Delay(10);
    HAL_SPI_Transmit(&hspi2,spiTXbuf,5,10);
    HAL_Delay(10);
    cs_reset_4();// hight
}
void init_drive(void)
{
    ////////////////////GCONF///////////////////////////////////////
    spiTXbuf[0]=0x80;
    spiTXbuf[1]=0x00;
    spiTXbuf[2]=0x00;
    spiTXbuf[3]=0x00;
    spiTXbuf[4]=0x03;
    
    spi_tr_m1();
    HAL_Delay(50);
    spi_tr_m2();
    HAL_Delay(50);
    spi_tr_m3();
    HAL_Delay(50);
    spi_tr_m4();  


    //////////////////////CHOPCONF//////////////////////////////////////
    spiTXbuf[0]=0xEC;//ec
    spiTXbuf[1]=0x00;
    spiTXbuf[2]=0x01;
    spiTXbuf[3]=0x00;
    spiTXbuf[4]=0xC3;
    
    spi_tr_m1();
    HAL_Delay(50);
    spi_tr_m2();
    HAL_Delay(50);
    spi_tr_m3();
    HAL_Delay(50);
    spi_tr_m4();   


    ///////////////////////IHOLD_IRUN////////////////////////////////////////
    spiTXbuf[0]=0xB0;//b0
    spiTXbuf[1]=0x00;
    spiTXbuf[2]=0x02;//06 ��� ���� ����
    spiTXbuf[3]=0x1D;
    spiTXbuf[4]=0x01;//05//��� �����
    
    spi_tr_m1();
    HAL_Delay(50);
    spi_tr_m2();
    HAL_Delay(50);
    spi_tr_m3();
    HAL_Delay(50);
    spi_tr_m4();    


   
}

void deinit_drive(void)
{
     ////////////////////GCONF///////////////////////////////////////
    spiTXbuf[0]=0x80;
    spiTXbuf[1]=0x00;
    spiTXbuf[2]=0x00;
    spiTXbuf[3]=0x00;
    spiTXbuf[4]=0x00;
    
    spi_tr_m1();
    HAL_Delay(50);
    spi_tr_m2();
    HAL_Delay(50);
    spi_tr_m3();
    HAL_Delay(50);
    spi_tr_m4();  
    
    //////////////////////CHOPCONF//////////////////////////////////////
    spiTXbuf[0]=0xEC;//ec
    spiTXbuf[1]=0x00;
    spiTXbuf[2]=0x00;
    spiTXbuf[3]=0x00;
    spiTXbuf[4]=0xC0;//c0
    
    spi_tr_m1();
    HAL_Delay(50);
    spi_tr_m2();
    HAL_Delay(50);
    spi_tr_m3();
    HAL_Delay(50);
    spi_tr_m4();   
    
    ///////////////////////IHOLD_IRUN////////////////////////////////////////
    spiTXbuf[0]=0xB0;//b0
    spiTXbuf[1]=0x00;
    spiTXbuf[2]=0x00;
    spiTXbuf[3]=0x00;
    spiTXbuf[4]=0x00;
    
    spi_tr_m1();
    HAL_Delay(50);
    spi_tr_m2();
    HAL_Delay(50);
    spi_tr_m3();
    HAL_Delay(50);
    spi_tr_m4();  
    
}

void Home(void)
{
    if(RxHeader.StdId==0x30)
      {
          
          if(RxHeader.DLC>0)
          {
            if(CAN_Rxdata[0]==0xFF)
            {
                HAL_TIM_Base_Stop(&htim1);
                HAL_TIM_Base_Stop_IT(&htim1);
            }
            if(CAN_Rxdata[1]==0xFF)
            {
                HAL_TIM_Base_Stop(&htim2);
                HAL_TIM_Base_Stop_IT(&htim2);
            }
            if(CAN_Rxdata[2]==0xFF)
            {
               HAL_TIM_Base_Stop(&htim3);
               HAL_TIM_Base_Stop_IT(&htim3); 
            }
            if(CAN_Rxdata[3]==0xFF)
            {
                HAL_TIM_Base_Stop(&htim4);
                HAL_TIM_Base_Stop_IT(&htim4);
            }
              RxHeader.StdId=0x00;
          }
          
          
          pHeader.StdId=0x4A0;
          pHeader.RTR=CAN_RTR_DATA;
            pHeader.IDE=CAN_ID_STD;
            pHeader.DLC=8;
            pHeader.TransmitGlobalTime=DISABLE;
          HAL_CAN_AddTxMessage(&hcan,&pHeader,CAN_Rxdata,&TxMailbox);
          
      }
}

void koord(void)
{
    if(RxHeader.StdId==0xA0)
      {
          
          
            for(int n=0; n<=8; n++)
           {
               CAN_Rxdata_kor[n]=CAN_Rxdata[n];
           }
                       
            
           //step_1=0;
           //step_2=0;
           //step_3=0;
           //step_4=0;
					 
            x_1= CAN_Rxdata_kor[0]*256 + CAN_Rxdata[1];
					  step_1=0;
            x_2= CAN_Rxdata_kor[4]*256 + CAN_Rxdata[5];
					  step_2=0;
            y_1= CAN_Rxdata_kor[2]*256 + CAN_Rxdata[3];
					  step_3=0;
            y_2= CAN_Rxdata_kor[6]*256 + CAN_Rxdata[7];
					  step_4=0;
					 step_1_f = 0;
					 step_2_f = 0;
           // x_1=/*(CAN_Rxdata_kor[0]<<8)|*/CAN_Rxdata[1];
            //x_2=/*(CAN_Rxdata_kor[4]<<8)|*/CAN_Rxdata[5];
            //y_1=/*(CAN_Rxdata_kor[2]<<8)|*/CAN_Rxdata[3];
            //y_2=/*(CAN_Rxdata_kor[6]<<8)|*/CAN_Rxdata[7];
           RxHeader.StdId=0x00;
       }

        
        
  //--------------------------------------------------------------------------------------------------------------------------------------      
    /*       if (x_1>0)
            {
            end_step_1=x_1*1000;                
			      pHeader.StdId=0x4A0;
            pHeader.RTR=CAN_RTR_DATA;
            pHeader.IDE=CAN_ID_STD;
            pHeader.DLC=0;
            pHeader.TransmitGlobalTime=DISABLE;           
            HAL_CAN_AddTxMessage(&hcan,&pHeader,CAN_Txdata,&TxMailbox);
            
                
               
  					HAL_TIM_Base_Start(&htim1);
            HAL_TIM_Base_Start_IT(&htim1);
                
             

						 

			*/
         /*   if(step_1>=end_step_1)
            {
                x_1=0;
                HAL_TIM_Base_Stop(&htim1);
                HAL_TIM_Base_Stop_IT(&htim1);
                end_step_1=0;
            pHeader.StdId=0xA0;
            pHeader.RTR=CAN_RTR_DATA;
            pHeader.IDE=CAN_ID_STD;
            pHeader.DLC=8;
            pHeader.TransmitGlobalTime=DISABLE;
            //TxData[0]=step;
            HAL_CAN_AddTxMessage(&hcan,&pHeader,CAN_Rxdata,&TxMailbox);
            }
					
        }
        */
            
 //------------------------------------------------------------------------------           
    /*        if (x_2>0)
		{
           
            end_step_2=x_2*1000;
            HAL_Delay(100);
			pHeader.StdId=0x4A0;
            pHeader.RTR=CAN_RTR_DATA;
            pHeader.IDE=CAN_ID_STD;
            pHeader.DLC=0;
            pHeader.TransmitGlobalTime=DISABLE;
     
            HAL_CAN_AddTxMessage(&hcan,&pHeader,CAN_Txdata,&TxMailbox);
            
         //       for(;step_2<=end_step_2;)
         //   {
         //       HAL_TIM_Base_Start(&htim2);
         //       HAL_TIM_Base_Start_IT(&htim2);
                //HAL_TIM_Base_Stop(&htim2);
                //HAL_TIM_Base_Stop_IT(&htim2);
         //   } 

             HAL_TIM_Base_Start(&htim2);
             HAL_TIM_Base_Start_IT(&htim2);
            

						
            if(step_2>=end_step_2)
            {
                x_2=0;
                HAL_TIM_Base_Stop(&htim2);
                HAL_TIM_Base_Stop_IT(&htim2);
                end_step_2=0;
            pHeader.StdId=0xA0;
            pHeader.RTR=CAN_RTR_DATA;
            pHeader.IDE=CAN_ID_STD;
            pHeader.DLC=8;
            pHeader.TransmitGlobalTime=DISABLE;
            //TxData[0]=step;
            HAL_CAN_AddTxMessage(&hcan,&pHeader,CAN_Rxdata_kor,&TxMailbox);
            }
        }
 */
 //----------------------------------------------------------------------------------------------------------------           
            
      
        
       
    

}

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */
   //init_drive();
    //HAL_Delay(500);
    
  /* USER CODE END 1 */
  

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */
   // 
  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_CAN_Init();
  MX_SPI2_Init();
  MX_TIM4_Init();
  MX_USART1_UART_Init();
  MX_TIM1_Init();
  MX_TIM2_Init();
  MX_TIM3_Init();
  /* USER CODE BEGIN 2 */
    HAL_GPIO_WritePin(CS1_GPIO_Port, CS1_Pin, GPIO_PIN_SET);
    HAL_GPIO_WritePin(CS2_GPIO_Port, CS2_Pin, GPIO_PIN_SET);
    HAL_GPIO_WritePin(CS3_GPIO_Port, CS3_Pin, GPIO_PIN_SET);
    HAL_GPIO_WritePin(CS4_GPIO_Port, CS4_Pin, GPIO_PIN_SET);
    init_drive();
    HAL_GPIO_WritePin(DIR1_GPIO_Port,DIR1_Pin,GPIO_PIN_SET);
    HAL_GPIO_WritePin(DIR2_GPIO_Port,DIR2_Pin,GPIO_PIN_SET);
    HAL_GPIO_WritePin(DIR3_GPIO_Port,DIR3_Pin,GPIO_PIN_SET);
    HAL_GPIO_WritePin(DIR4_GPIO_Port,DIR4_Pin,GPIO_PIN_SET);
    
    
   
   
  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {
      
      
      if(HAL_CAN_GetRxMessage(&hcan,CAN_RX_FIFO0,&RxHeader,CAN_Rxdata) !=HAL_OK)
		{
            Error_Handler();
	}
        
       /* 
        if(HAL_CAN_GetRxMessage(&hcan,CAN_RX_FIFO1,&RxHeader,CAN_Rxdata) !=HAL_OK)
		{
            Error_Handler();
		}
        */
      
      Home();
      koord();
		
		
		
//---------------------------------------------------------------------------------------------------------------------------------
      
if (x_1>0)
            {
            end_step_1=x_1*1000;                
			pHeader.StdId=0x4A0;
            pHeader.RTR=CAN_RTR_DATA;
            pHeader.IDE=CAN_ID_STD;
            pHeader.DLC=0;
            pHeader.TransmitGlobalTime=DISABLE;           
            HAL_CAN_AddTxMessage(&hcan,&pHeader,CAN_Txdata,&TxMailbox);
            
                
               
  			HAL_TIM_Base_Start(&htim1);
            HAL_TIM_Base_Start_IT(&htim1);
                
            
						 

			
            if(step_1_f==1)
            {
                //x_1=0;
                HAL_TIM_Base_Stop(&htim1);
                HAL_TIM_Base_Stop_IT(&htim1);
							  HAL_TIM_IRQHandler(&htim1);
               // end_step_1=0;
            pHeader.StdId=0xA0;
            pHeader.RTR=CAN_RTR_DATA;
            pHeader.IDE=CAN_ID_STD;
            pHeader.DLC=8;
            pHeader.TransmitGlobalTime=DISABLE;
            //TxData[0]=step;
            HAL_CAN_AddTxMessage(&hcan,&pHeader,CAN_Rxdata,&TxMailbox);
            }	
					}
					
					
		
		//------------------------------------------------------------------------------           
         


		if (x_2>0)
		{
           
            end_step_2=x_2*1000;
            //HAL_Delay(100);
			pHeader.StdId=0x4A0;
            pHeader.RTR=CAN_RTR_DATA;
            pHeader.IDE=CAN_ID_STD;
            pHeader.DLC=0;
            pHeader.TransmitGlobalTime=DISABLE;
            HAL_CAN_AddTxMessage(&hcan,&pHeader,CAN_Txdata,&TxMailbox);
            
         //       for(;step_2<=end_step_2;)
         //   {
         //       HAL_TIM_Base_Start(&htim2);
         //       HAL_TIM_Base_Start_IT(&htim2);
               // HAL_TIM_Base_Stop(&htim2);
                //HAL_TIM_Base_Stop_IT(&htim2);
         //   } 
             
             HAL_TIM_Base_Start(&htim2);
             HAL_TIM_Base_Start_IT(&htim2);
             

						
            if(step_2_f==1)
            {
               // x_2=0;
                HAL_TIM_Base_Stop(&htim2);
                HAL_TIM_Base_Stop_IT(&htim2);
						  	HAL_TIM_IRQHandler(&htim1); 
               // end_step_2=0;
                pHeader.StdId=0xA0;
                pHeader.RTR=CAN_RTR_DATA;
                pHeader.IDE=CAN_ID_STD;
                pHeader.DLC=8;
                pHeader.TransmitGlobalTime=DISABLE;
                //TxData[0]=step;
                HAL_CAN_AddTxMessage(&hcan,&pHeader,CAN_Rxdata_kor,&TxMailbox);
            }
        }
        
     






//-----------------------------------------------------------------------------------------------------------
























      
       
    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

  /** Initializes the CPU, AHB and APB busses clocks 
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_ON;
  RCC_OscInitStruct.HSEPredivValue = RCC_HSE_PREDIV_DIV1;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLMUL = RCC_PLL_MUL8;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB busses clocks 
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV2;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_2) != HAL_OK)
  {
    Error_Handler();
  }
}

/**
  * @brief CAN Initialization Function
  * @param None
  * @retval None
  */
static void MX_CAN_Init(void)
{

  /* USER CODE BEGIN CAN_Init 0 */

  /* USER CODE END CAN_Init 0 */

  /* USER CODE BEGIN CAN_Init 1 */

  /* USER CODE END CAN_Init 1 */
  hcan.Instance = CAN1;
  hcan.Init.Prescaler = 4;
  hcan.Init.Mode = CAN_MODE_NORMAL;
  hcan.Init.SyncJumpWidth = CAN_SJW_2TQ;
  hcan.Init.TimeSeg1 = CAN_BS1_13TQ;
  hcan.Init.TimeSeg2 = CAN_BS2_2TQ;
  hcan.Init.TimeTriggeredMode = DISABLE;
  hcan.Init.AutoBusOff = DISABLE;
  hcan.Init.AutoWakeUp = DISABLE;
  hcan.Init.AutoRetransmission = DISABLE;
  hcan.Init.ReceiveFifoLocked = DISABLE;
  hcan.Init.TransmitFifoPriority = DISABLE;
  if (HAL_CAN_Init(&hcan) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN CAN_Init 2 */
    sFilterConfig.FilterBank=0;
    sFilterConfig.FilterMode=CAN_FILTERMODE_IDLIST;
    sFilterConfig.FilterScale=CAN_FILTERSCALE_32BIT;
    sFilterConfig.FilterIdHigh=0xA0<<5; 
	sFilterConfig.FilterIdLow=0x00;
	sFilterConfig.FilterMaskIdHigh=0xA0<<5;
	sFilterConfig.FilterMaskIdLow=0x00;
	sFilterConfig.FilterFIFOAssignment=CAN_RX_FIFO0;
	sFilterConfig.FilterActivation=ENABLE;
    
   // HAL_CAN_ActivateNotification(&hcan, CAN_IT_RX_FIFO0_MSG_PENDING);
  
    HAL_CAN_ConfigFilter(&hcan, &sFilterConfig);
  
    sFilterConfig.FilterBank=1;
    sFilterConfig.FilterMode=CAN_FILTERMODE_IDLIST;
    sFilterConfig.FilterScale=CAN_FILTERSCALE_32BIT;
    sFilterConfig.FilterIdHigh=0x30<<5; 
	sFilterConfig.FilterIdLow=0x00;
	sFilterConfig.FilterMaskIdHigh=0x30<<5;
	sFilterConfig.FilterMaskIdLow=0x00;
	sFilterConfig.FilterFIFOAssignment=CAN_RX_FIFO0;
	sFilterConfig.FilterActivation=ENABLE;
  
    //HAL_CAN_ActivateNotification(&hcan, CAN_IT_RX_FIFO1_MSG_PENDING);
  
	HAL_CAN_ConfigFilter(&hcan, &sFilterConfig);
    HAL_CAN_Start(&hcan); //start CAN
    HAL_CAN_ActivateNotification(&hcan, CAN_IT_RX_FIFO0_MSG_PENDING);
	
    //
   // HAL_CAN_ActivateNotification(&hcan, CAN_IT_RX_FIFO0_MSG_PENDING | CAN_IT_TX_MAILBOX_EMPTY);
    HAL_CAN_ActivateNotification(&hcan, CAN_IT_TX_MAILBOX_EMPTY);
    
  /* USER CODE END CAN_Init 2 */

}

/**
  * @brief SPI2 Initialization Function
  * @param None
  * @retval None
  */
static void MX_SPI2_Init(void)
{

  /* USER CODE BEGIN SPI2_Init 0 */

  /* USER CODE END SPI2_Init 0 */

  /* USER CODE BEGIN SPI2_Init 1 */

  /* USER CODE END SPI2_Init 1 */
  /* SPI2 parameter configuration*/
  hspi2.Instance = SPI2;
  hspi2.Init.Mode = SPI_MODE_MASTER;
  hspi2.Init.Direction = SPI_DIRECTION_2LINES;
  hspi2.Init.DataSize = SPI_DATASIZE_8BIT;
  hspi2.Init.CLKPolarity = SPI_POLARITY_HIGH;
  hspi2.Init.CLKPhase = SPI_PHASE_2EDGE;
  hspi2.Init.NSS = SPI_NSS_SOFT;
  hspi2.Init.BaudRatePrescaler = SPI_BAUDRATEPRESCALER_16;
  hspi2.Init.FirstBit = SPI_FIRSTBIT_MSB;
  hspi2.Init.TIMode = SPI_TIMODE_DISABLE;
  hspi2.Init.CRCCalculation = SPI_CRCCALCULATION_DISABLE;
  hspi2.Init.CRCPolynomial = 10;
  if (HAL_SPI_Init(&hspi2) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN SPI2_Init 2 */

  /* USER CODE END SPI2_Init 2 */

}

/**
  * @brief TIM1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM1_Init(void)
{

  /* USER CODE BEGIN TIM1_Init 0 */

  /* USER CODE END TIM1_Init 0 */

  TIM_ClockConfigTypeDef sClockSourceConfig = {0};
  TIM_MasterConfigTypeDef sMasterConfig = {0};

  /* USER CODE BEGIN TIM1_Init 1 */

  /* USER CODE END TIM1_Init 1 */
  htim1.Instance = TIM1;
  htim1.Init.Prescaler = 100;
  htim1.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim1.Init.Period = 1;
  htim1.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim1.Init.RepetitionCounter = 0;
  htim1.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_Base_Init(&htim1) != HAL_OK)
  {
    Error_Handler();
  }
  sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
  if (HAL_TIM_ConfigClockSource(&htim1, &sClockSourceConfig) != HAL_OK)
  {
    Error_Handler();
  }
  sMasterConfig.MasterOutputTrigger = TIM_TRGO_UPDATE;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim1, &sMasterConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM1_Init 2 */

  /* USER CODE END TIM1_Init 2 */

}

/**
  * @brief TIM2 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM2_Init(void)
{

  /* USER CODE BEGIN TIM2_Init 0 */

  /* USER CODE END TIM2_Init 0 */

  TIM_ClockConfigTypeDef sClockSourceConfig = {0};
  TIM_MasterConfigTypeDef sMasterConfig = {0};

  /* USER CODE BEGIN TIM2_Init 1 */

  /* USER CODE END TIM2_Init 1 */
  htim2.Instance = TIM2;
  htim2.Init.Prescaler = 100;
  htim2.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim2.Init.Period = 1;
  htim2.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim2.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_Base_Init(&htim2) != HAL_OK)
  {
    Error_Handler();
  }
  sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
  if (HAL_TIM_ConfigClockSource(&htim2, &sClockSourceConfig) != HAL_OK)
  {
    Error_Handler();
  }
  sMasterConfig.MasterOutputTrigger = TIM_TRGO_UPDATE;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim2, &sMasterConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM2_Init 2 */

  /* USER CODE END TIM2_Init 2 */

}

/**
  * @brief TIM3 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM3_Init(void)
{

  /* USER CODE BEGIN TIM3_Init 0 */

  /* USER CODE END TIM3_Init 0 */

  TIM_ClockConfigTypeDef sClockSourceConfig = {0};
  TIM_MasterConfigTypeDef sMasterConfig = {0};

  /* USER CODE BEGIN TIM3_Init 1 */

  /* USER CODE END TIM3_Init 1 */
  htim3.Instance = TIM3;
  htim3.Init.Prescaler = 100;
  htim3.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim3.Init.Period = 1;
  htim3.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim3.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_Base_Init(&htim3) != HAL_OK)
  {
    Error_Handler();
  }
  sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
  if (HAL_TIM_ConfigClockSource(&htim3, &sClockSourceConfig) != HAL_OK)
  {
    Error_Handler();
  }
  sMasterConfig.MasterOutputTrigger = TIM_TRGO_UPDATE;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim3, &sMasterConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM3_Init 2 */

  /* USER CODE END TIM3_Init 2 */

}

/**
  * @brief TIM4 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM4_Init(void)
{

  /* USER CODE BEGIN TIM4_Init 0 */

  /* USER CODE END TIM4_Init 0 */

  TIM_ClockConfigTypeDef sClockSourceConfig = {0};
  TIM_MasterConfigTypeDef sMasterConfig = {0};

  /* USER CODE BEGIN TIM4_Init 1 */

  /* USER CODE END TIM4_Init 1 */
  htim4.Instance = TIM4;
  htim4.Init.Prescaler = 100;
  htim4.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim4.Init.Period = 1;
  htim4.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim4.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_Base_Init(&htim4) != HAL_OK)
  {
    Error_Handler();
  }
  sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
  if (HAL_TIM_ConfigClockSource(&htim4, &sClockSourceConfig) != HAL_OK)
  {
    Error_Handler();
  }
  sMasterConfig.MasterOutputTrigger = TIM_TRGO_UPDATE;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim4, &sMasterConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM4_Init 2 */

  /* USER CODE END TIM4_Init 2 */

}

/**
  * @brief USART1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_USART1_UART_Init(void)
{

  /* USER CODE BEGIN USART1_Init 0 */

  /* USER CODE END USART1_Init 0 */

  /* USER CODE BEGIN USART1_Init 1 */

  /* USER CODE END USART1_Init 1 */
  huart1.Instance = USART1;
  huart1.Init.BaudRate = 115200;
  huart1.Init.WordLength = UART_WORDLENGTH_8B;
  huart1.Init.StopBits = UART_STOPBITS_1;
  huart1.Init.Parity = UART_PARITY_NONE;
  huart1.Init.Mode = UART_MODE_TX_RX;
  huart1.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart1.Init.OverSampling = UART_OVERSAMPLING_16;
  if (HAL_UART_Init(&huart1) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN USART1_Init 2 */

  /* USER CODE END USART1_Init 2 */

}

/**
  * @brief GPIO Initialization Function
  * @param None
  * @retval None
  */
static void MX_GPIO_Init(void)
{
  GPIO_InitTypeDef GPIO_InitStruct = {0};

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOD_CLK_ENABLE();
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOA, DIR1_Pin|STEP1_Pin|CS1_Pin|DIR3_Pin 
                          |STEP3_Pin|RE_DE_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOB, CS3_Pin|DIR2_Pin|CS4_Pin|DIR4_Pin 
                          |STEP4_Pin|STEP2_Pin|CS2_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pins : DIR1_Pin STEP1_Pin CS1_Pin DIR3_Pin 
                           STEP3_Pin RE_DE_Pin */
  GPIO_InitStruct.Pin = DIR1_Pin|STEP1_Pin|CS1_Pin|DIR3_Pin 
                          |STEP3_Pin|RE_DE_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

  /*Configure GPIO pins : CS3_Pin DIR2_Pin CS4_Pin DIR4_Pin 
                           STEP4_Pin STEP2_Pin CS2_Pin */
  GPIO_InitStruct.Pin = CS3_Pin|DIR2_Pin|CS4_Pin|DIR4_Pin 
                          |STEP4_Pin|STEP2_Pin|CS2_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

}

/* USER CODE BEGIN 4 */

/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */

  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{ 
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
